import React from 'react';
import './App.css';

import Pokedex from './components/Pokedex/Pokedex';
import Error from './components/Error/Error';
import Details from './components/Details/Details';
import {BrowserRouter as Router, NavLink, Route, Routes} from 'react-router-dom' 

function App() {
  return (
    <Router>
      <div className="App">
        <header>
          <nav>
            <ul>
              {/* <li><NavLink to="/">Início</NavLink></li> */}
            </ul>
          </nav>
        </header>
        <main>
          <Routes>
            <Route path="/details/:name" element={<Details/>}/>
            <Route path="/" exact element={<Pokedex/>}/>
            <Route path="*" exact element={<Error/>}/>
          </Routes>
        </main>
      </div>
    </Router>
  );
}

export default App;
