import React from 'react'
import { Link } from 'react-router-dom'
import './Pokemon.css'

function Pokemon(props) {
  return (
    <div className="container">
      <div className="Pokemon">
        <ul className="UlMedia">
          <li>
            <div className="Card">
              <strong>{props.pokemon.name}</strong>
              <br/>
              <ul>
                <li><Link to={`/details/${props.pokemon.name}`}>Details</Link></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Pokemon