import React, { useState, useEffect } from 'react'

import Detail from '../Pokemon/Pokemon'
import Pagination from '../Utils/Pagination/Pagination'
import './Pokedex.css'
function Pokedex(){

    const [pokemons, setPokemons] = useState([])
    const [nextPageUrl, setNextPageUrl] = useState();
    const [prevPageUrl, setPrevPageUrl] = useState();
    const [currentPageUrl, setCurrentPageUrl] = useState(
      "https://pokeapi.co/api/v2/pokemon"
    );

    useEffect(() => {
        fetch(currentPageUrl)
        .then(response => response.json())
        .then(data => {
            const pokemons = data.results.map((pokemon, index) => ({
              id: index,
              name: pokemon.name,
            }))
            setNextPageUrl(data.next);
            setPrevPageUrl(data.previous);
            setPokemons(pokemons)
        })
    }, [currentPageUrl])

    function gotoNextPage() {
      setCurrentPageUrl(nextPageUrl);
    }
  
    function gotoPrevPage() {
      setCurrentPageUrl(prevPageUrl);
    }

    return (
        <>    
        <div className="Main">
          {pokemons.map(pokemon => (
            <Detail key={pokemon.id}
            pokemon={pokemon}
            />
          ))}
        </div>
        <div>
          <Pagination gotoNextPage={nextPageUrl ? gotoNextPage : null} gotoPrevPage={prevPageUrl ? gotoPrevPage : null}/>
        </div>
        </>
      )
}

export default Pokedex