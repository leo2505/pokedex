import React, { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom';
import ProgressBar from '../Utils/ProgressBar/ProgressBar';
import './Details.css'

function Details() {
    const {name} = useParams();
    const [details, setDetails] = useState([])
    const [image, setImage] = useState('')

    useEffect(() => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
        .then(response => response.json())
        .then(data => {
            const details = {
              id: data.id,
              name: data.name,
              weight: data.weight,
              height: data.height,
              stats: data.stats,
              abilities: data.abilities,
              images: data.sprites,
              types: data.types
            }
            setImage(details.images.front_default)
            setDetails(details)
        })

    }, [])
    
    const changePosition = () => {
        setImage(state => state === details.images.front_default ? details.images.back_default : details.images.front_default)
    }
  return (
    <>
        <Link to="/">Voltar</Link> 
        <div className="Detail">
            <div className="Row Name">
                {details.name} | nº {details.id}
            </div>
            <div className="Row">
                <img src={image} alt={details.name} />
                <button className="Rotate" onClick={changePosition}>🔄</button>
            </div>
            <div className="Row">
                WT: {(details.weight * 0.453592).toFixed(1)} kg - HT: {details.height*2.54.toFixed(1)} cm
            </div>
            <div className="Row">
                <p><strong>Abilities</strong></p>
                <ul>
                    {details.abilities && details.abilities.map((detail, index) => (<li key={index}>{detail.ability.name}</li>))}
                </ul>
            </div>
            <div className="Row">
                <p><strong>Types</strong></p>
                <ul>
                    {details.types && details.types.map((detail, index) => (<li key={index}>{detail.type.name}</li>))}
                </ul>
            </div>
            <div className="Row">
                <p><strong>Stats</strong></p>
                <ul>
                    {details.stats && details.stats.map((detail, index) => (<li className="Stats" key={index}>{detail.stat.name} <br/> <ProgressBar detail={detail}/></li>))}
                </ul>
            </div>
        </div>
    </>
  )
}

export default Details