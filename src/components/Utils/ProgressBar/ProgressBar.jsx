import React, { useState, useEffect } from 'react'

function ProgressBar(props){
    // const {name} = useParams();
    const [color, setColor] = useState([])
    useEffect(() => {
        if(props.detail.base_stat <= 25){
            const color = {progress: 'progress-danger'};
            setColor(color)
        }
        if(props.detail.base_stat > 25 && props.detail.base_stat < 75){
            const color = {progress: 'progress-alert'}
            setColor(color)
        }
        if(props.detail.base_stat >= 75 && props.detail.base_stat <= 100){
            const color = {progress: 'progress-full'}
            setColor(color)
        }
        if(props.detail.base_stat > 100){
            const color = {progress: 'progress-best'}
            setColor(color)
        }
    }, [])
    
    return(
        <>
        <progress className={color.progress} value={props.detail.base_stat} max="100" data-label={`${props.detail.base_stat}`}></progress>
        </>
    )
}
export default ProgressBar;