import React from "react";

import './Pagination.css'

function Pagination({ gotoNextPage, gotoPrevPage }) {
  return (
    <div className="PreviousNext">
      {gotoPrevPage && <button className="PrevNextBtn" onClick={gotoPrevPage}>&#8249;</button>}
      {gotoNextPage && <button className="PrevNextBtn" onClick={gotoNextPage}>&#8250;</button>}
    </div>
  );
}

export default Pagination
