
# Documentação

Para rodar o projeto é necessário primeiramente instalar as dependências
### `npm install`

Depois é só startar
### `npm start`

### Regras: 

✓ Página principal listando os pokemons (PLUS: Paginação)

✓ Redirecionar para detalhamento do pokemon

✓ Intercalar posição de frente e costas do pokémon no clique

✓ Altura em cm (com conversão)

✓ Peso em kg (com conversão)

✓ Status Base (Coloquei um progress bar com cor dinâmica, acima de 100 fica azul)

✓ Habilidades

✓ Tipos

✓ Com um botão de voltar

✓ Não foram utilizadas nenhuma biblioteca de layout, apenas dependências para rodar o projeto, inclusive o próprio fetch da API é feita com a implementação do último ECS ao invés da biblioteca AXIOS.

✓ 100% testado no Chrome

✓ Todo em inglês

## Critérios Técnicos
✓ Código Fonte em inglês

✓ Em React

✓ Sintaxe ES6

✓ Componentes que não são navegáveis, estão dentro da pasta Utils, como por exemplo a paginação.

### Dos desejados:
✓ Hooks (Não foram utilizadas classes)

✓ Router (Com react-router-dom)

X Testes Unitários (não consegui instalar o Enzyme junto com o Jest pois o adapter dele estava dando erro, já trabalhei com teste mas backend, então acredito que esse Enzyme seja necessário para renderizar componentes de tela)

✓ Transpiladores JS (Acredito que esse critério sejam Arquivos JSX, Arrow Functions...)

X Redux (Não encontrei uma forma de aplicar o Redux nesse desafio, geralmente uso em botões de add, remover...)